import { NgModule } from '@angular/core';
import { MyCustomNavigationComponent } from './my-custom-navigation.component';



@NgModule({
  declarations: [MyCustomNavigationComponent],
  imports: [
  ],
  exports: [MyCustomNavigationComponent]
})
export class MyCustomNavigationModule { }
