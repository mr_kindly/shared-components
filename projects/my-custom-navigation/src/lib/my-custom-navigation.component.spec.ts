import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCustomNavigationComponent } from './my-custom-navigation.component';

describe('MyCustomNavigationComponent', () => {
  let component: MyCustomNavigationComponent;
  let fixture: ComponentFixture<MyCustomNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCustomNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCustomNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
