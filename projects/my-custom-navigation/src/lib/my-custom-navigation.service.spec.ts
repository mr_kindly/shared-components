import { TestBed } from '@angular/core/testing';

import { MyCustomNavigationService } from './my-custom-navigation.service';

describe('MyCustomNavigationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyCustomNavigationService = TestBed.get(MyCustomNavigationService);
    expect(service).toBeTruthy();
  });
});
