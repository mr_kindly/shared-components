import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyCustomHeaderComponentComponent } from './my-custom-header-component.component';

describe('MyCustomHeaderComponentComponent', () => {
  let component: MyCustomHeaderComponentComponent;
  let fixture: ComponentFixture<MyCustomHeaderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyCustomHeaderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyCustomHeaderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
