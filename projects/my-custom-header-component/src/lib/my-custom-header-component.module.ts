import { NgModule } from '@angular/core';
import { MyCustomHeaderComponentComponent } from './my-custom-header-component.component';



@NgModule({
  declarations: [MyCustomHeaderComponentComponent],
  imports: [
  ],
  exports: [MyCustomHeaderComponentComponent]
})
export class MyCustomHeaderComponentModule { }
