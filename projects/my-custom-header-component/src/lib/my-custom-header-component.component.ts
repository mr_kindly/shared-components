import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'my-custom-header-component',
  templateUrl: 'my-custom-header-component.html',
  styleUrls: ['my-custom-header-component.component.scss']
})
export class MyCustomHeaderComponentComponent implements OnInit {
  public pageTitle: string;
  public authenticatedUser: any;

  constructor() { }

  ngOnInit() {
    this.authenticatedUser = {
      userId: 1,
      login: 'test_login',
      firstName: 'Firstname',
      lastName: 'Lastname',
      email: 'test@email.com'
    };

    this.pageTitle = 'test';
  }

}
