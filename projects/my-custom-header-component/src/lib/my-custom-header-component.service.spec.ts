import { TestBed } from '@angular/core/testing';

import { MyCustomHeaderComponentService } from './my-custom-header-component.service';

describe('MyCustomHeaderComponentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MyCustomHeaderComponentService = TestBed.get(MyCustomHeaderComponentService);
    expect(service).toBeTruthy();
  });
});
