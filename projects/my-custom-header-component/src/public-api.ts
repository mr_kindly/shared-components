/*
 * Public API Surface of my-custom-header-component
 */

export * from './lib/my-custom-header-component.service';
export * from './lib/my-custom-header-component.component';
export * from './lib/my-custom-header-component.module';
